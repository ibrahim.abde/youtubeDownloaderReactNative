/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    ProgressBarAndroid
} from 'react-native';

import downloader from './ytdlBundle'
import RNFS from 'react-native-fs';

export default class App extends Component<{}> {

    constructor(props) {
        super(props);
        this.state = { link: 'https://www.youtube.com/watch?v=Ur22H1LjiuA', progress: 0 };

        this.download = this.download.bind(this);
    }

    download() {
        downloader.getInfo(this.state.link, (err, info) => {
            if (err) throw err;

            var format = downloader.chooseFormat(info.formats, { quality: '160' });
            console.log('download started');


            const downloadOptions = {
                fromUrl: format.url,
                toFile: RNFS.ExternalStorageDirectoryPath + '/video.mp4',
                progress: (res) => this.setState({ progress: res.bytesWritten / res.contentLength })
            };

            RNFS.downloadFile(downloadOptions).promise.then((res) => {
                console.log('Download finished');
                this.setState({ progress: 1 });    
            })
            .catch((err) => {
                console.log('DownloadFile Error', err);
            });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Download Link :
        </Text>
                <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}

                    onChangeText={(text) => this.setState({ link: text })}

                    value={this.state.link}
                />
                <Button
                    onPress={this.download}
                    title="Download"
                    color="#841584"
                />

                <ProgressBarAndroid styleAttr="Horizontal" indeterminate={false} progress={this.state.progress} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
